# Fizzbuzz on steroids 🤪

We're going to create a Fizzbuzz game, a quintisential exercise of programming. We'll add testing to it and a CI pipeline to add tests to. 

Because of the CI pipeline, we'll start by making some unit tests and employ Test-Driven Development. We'll add these tests to our pipeline and then focus on the code.

Things we'll cover:
- Git and markdown
- Python Functional Programming (loops etc)
- TTD
- CI (continuous integration)
- Mini scrum project: sprints, user stories, and definition of done

### Definition of done

What things do we need to check that are communual to all user stories?
- All acceptance criteria passed

### User stories

Rules:
- If it's a multiple of 3, I sholud get 3
- If it's a multiple of 5, I should get buzz
- If it's a multiple of 15, I should get 'fizzbuzz'

**Core game logic user stories**
**User story 1:** As a player, when I give the game a multiple of 3, the game should output 'fizz' so that it follows the game logic.

**User story 2:** As a player, when I give the game a multiple of 5, the game should output 'buzz' so that it follows the game logic.

**User story 3:** As a player, when I give the game a multiple of 15, the game should output 'fizzbuzz' so that it follows the game logic.

**User story 4:** As a player, when I give the game a number that is neither a multiple of 3 or 5, the game should output the input.

**Extra user stories**
To come later


As a player, I should be prompted for an input, to start the game.

As a player, I should see all the numbers up to and including the number I chose while following the rules of the game. 

As a player, I should be able to keep playing until I use the word: 'exit' or something you define.



### Plan/ Psudo Code

TDD plan and add CI pipeline

1) Write tests
2) Write code
3) Pass the tests
4) Move onto setup of a CI pipeline

**Step 1: Write tests** 

- Use user stories to write test
- Write tests using pytest

We'll need these three functions:
- `check_factor_three` -- returns true or false
- `check_factor_five` -- returns true or false
- `check_factor_fifteen` -- returns true or false 


Then implement these functions (user stories 1, 2 and 3).

### Best practices in python

1) Dry code (don't repeat yourself). Use functions to have less repetition in your code. E.g. check_15 calls check3 and check5. 
2) Don't print in functions because the final output is none. What this means is, if you were to test, the output would always be none and no tests would pass.
3) Separation of concerns and clean code. Segregate your code per sections e.g. definition of functions, running functions and tests. In the file with the definition of the functions, don't call the function otherwise when you import the function in another file, you will also be calling the functions and everything else that comes with it. 
4) Keep functions small. Makes them testable and reusable.