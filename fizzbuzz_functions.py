# Here we need to make fizzbuzz functions

# Function to test if number is divisible by 3
# We first need to deifne the function, check_factor_three
# Next, need to take in one argument
# Needs to assert if that argument is divisible by 3
# If it is return true, else return false

def check_factor_three(number):
    if number % 3 == 0:
        return(True)
    else:
        return(False)

# print(check_factor_three(6))
# print(type(check_factor_three(6))) 

def check_factor_five(number):
    if number % 5 == 0:
        return(True)
    else:
        return(False)

#num = 78
#print(check_factor_five(num))

def check_factor_three_and_five(number):
    if check_factor_three(number) and check_factor_five(number):
        return(True)
    else:
        return(False)

# print(check_factor_three_and_five(19)) 


def fizzbuzz_game(number):
    if check_factor_three_and_five(number):
        return("Fizzbuzz")
    elif check_factor_five(number):
        return("Buzz")
    elif check_factor_three(number):
        return("Fizz")
    else:
        return(number) 


# print(fizzbuzz_game(30)) 

def check_factor_four(number):
    if number % 4 == 0:
        return(True)
    else:
        return(False)