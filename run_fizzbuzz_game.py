from fizzbuzz_functions import * 

# We want our game to run continuously until we say the magic word (pineapple)
# We want the game to prompt us for input and then play! :)
# If we say pineapple, it will exit

while True:
    user_input=input("Give me a number pls: ")
    if user_input == "Pineapple":
        break 
    print(fizzbuzz_game(int(user_input)))

